sheep for Nadesiko
============

Nadesiko, なでしこ, is Japanese Natural Language Programing Language.

See more details, http://nadesi.com/top/

Nadesiko is JAPANESE ONLY.

How to build
------------

1. Download Nadesiko from http://nadesi.com/top/index.php?%E3%83%80%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%89

2. Launch `nakopad.exe` which is in downloaded zip archive.

3. Click menu [実行(R)] -> [実行(R) F5]

License
------------
WTFPL

